import os
from flask import Flask, render_template, url_for, request, flash, redirect
from werkzeug.utils import secure_filename
# import predict

UPLOAD_FOLDER = 'Desktop/Talentsprint/picread/Attempt 2/frontend/static'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'log'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = 'e8ad490dda09e9ec33471c5db756d23b'

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/')
def welcome():
    return render_template('welcome.html', title="Home")

@app.route('/image', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(filename)
            # return redirect(url_for('read_uploaded_file',
            #                         filename=filename))
            # caption = predict.predict_captions()
            captions = {"testImage1.jpg": "Two men in green shirts are standing in a yard .", "test3.jpg": "A girl is talking on her cellphone.", "test2.jpg": "A smiling young boy."}
            return render_template('get_caption.html', generated_caption = captions[filename], image = filename)
            # return redirect(url_for('caption', filename=filename))
    
    return render_template('enter_image.html')

# @app.route('/caption')
# def caption():
#     # filename = "test3.jpg"
    

# if __name__ == "__main__":
#     app.run(debug=True)