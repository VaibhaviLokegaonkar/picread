from flask import Flask, render_template, url_for

app = Flask(__name__)

@app.route('/')
def welcome():
    return render_template('welcome.html', title="Home")

@app.route('/enter_image')
def enter_image():
    return render_template('input_image.html')

if __name__ == "__main__":
    app.run(debug=True)