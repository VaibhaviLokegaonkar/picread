{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "predict.ipynb",
      "provenance": [],
      "collapsed_sections": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "accelerator": "GPU"
  },
  "cells": [
    {
      "cell_type": "code",
      "metadata": {
        "id": "sFuJ6RE82yso",
        "colab_type": "code",
        "colab": {
          "base_uri": "https://localhost:8080/",
          "height": 121
        },
        "outputId": "c17cf078-06e3-4867-f739-b0f0ec8cc1e9"
      },
      "source": [
        "from google.colab import drive\n",
        "drive.mount('/content/gdrive')"
      ],
      "execution_count": null,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "Go to this URL in a browser: https://accounts.google.com/o/oauth2/auth?client_id=947318989803-6bn6qk8qdgf4n4g3pfee6491hc0brc4i.apps.googleusercontent.com&redirect_uri=urn%3aietf%3awg%3aoauth%3a2.0%3aoob&scope=email%20https%3a%2f%2fwww.googleapis.com%2fauth%2fdocs.test%20https%3a%2f%2fwww.googleapis.com%2fauth%2fdrive%20https%3a%2f%2fwww.googleapis.com%2fauth%2fdrive.photos.readonly%20https%3a%2f%2fwww.googleapis.com%2fauth%2fpeopleapi.readonly&response_type=code\n",
            "\n",
            "Enter your authorization code:\n",
            "··········\n",
            "Mounted at /content/gdrive\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "yu9nIsSEDE5M",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "import matplotlib.pyplot as plt\n",
        "import pandas as pd\n",
        "import pickle\n",
        "import numpy as np\n",
        "import os\n",
        "from keras.applications.resnet50 import ResNet50\n",
        "from keras.optimizers import Adam\n",
        "from keras.layers import Dense, Flatten,Input, Convolution2D, Dropout, LSTM, TimeDistributed, Embedding, Bidirectional, Activation, RepeatVector,Concatenate\n",
        "from keras.models import Sequential, Model, load_model\n",
        "from keras.utils import np_utils\n",
        "import random\n",
        "from keras.preprocessing import image, sequence\n",
        "import matplotlib.pyplot as plt"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "BcNPyafkO1Y6",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "model = load_model(\"/content/model.h5\")"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "P4ZWFeNL-M08",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "word_2_indices = pickle.load(open('/content/drive/My Drive/Colab Notebooks/word_2_indices.pkl', 'rb'))\n",
        "indices_2_word = pickle.load(open('/content/drive/My Drive/Colab Notebooks/indices_2_word.pkl', 'rb'))"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "hbtTrUVNKh_Z",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "def preprocessing(img_path):\n",
        "    im = image.load_img(img_path, target_size=(224,224,3))\n",
        "    im = image.img_to_array(im)\n",
        "    im = np.expand_dims(im, axis=0)\n",
        "    return im"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "O0NeQPwb2Zlr",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "def get_encoding(model, img):\n",
        "    image = preprocessing(img)\n",
        "    pred = model.predict(image).reshape(2048)\n",
        "    return pred"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "bMDz4g6kD-PC",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "resnet = ResNet50(include_top=False,weights='imagenet',input_shape=(224,224,3),pooling='avg')"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "YH619IpnEC44",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "img = \"/content/drive/My Drive/Colab Notebooks/Flickr8k/Flickr8k_Dataset/Flicker8k_Dataset/1453366750_6e8cf601bf.jpg\"\n",
        "\n",
        "test_img = get_encoding(resnet, img)"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "jCEXAnw7FlGk",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "max_len = 40"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "hK-hRLjRHILB",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "def predict_captions(image):\n",
        "    start_word = [\"<start>\"]\n",
        "    while True:\n",
        "        par_caps = [word_2_indices[i] for i in start_word]\n",
        "        par_caps = sequence.pad_sequences([par_caps], maxlen=max_len, padding='post')\n",
        "        preds = model.predict([np.array([image]), np.array(par_caps)])\n",
        "        word_pred = indices_2_word[np.argmax(preds[0])]\n",
        "        start_word.append(word_pred)\n",
        "        \n",
        "        if word_pred == \"<end>\" or len(start_word) > max_len:\n",
        "            break\n",
        "            \n",
        "    return ' '.join(start_word[1:-1])\n",
        "\n",
        "Argmax_Search = predict_captions(test_img)"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "q3MPafhV6BHn",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "from nltk.translate.bleu_score import corpus_bleu\n",
        "\n",
        "TOKEN_FILE = \"/content/drive/My Drive/Colab Notebooks/Flickr8k/Flickr8k_text/Flickr8k.token.txt\"\n",
        "def getIMageID(TOKEN_FILE):\n",
        "  return set([line.split()[0].split(\".\")[0] for line in open(TOKEN_FILE, 'r').readlines()])\n",
        "\n",
        "def getCaptions(TOKEN_FILE):\n",
        "  captions, all_captions = [], []\n",
        "  for line in open(TOKEN_FILE, 'r').readlines():\n",
        "    id, caption = line.split()[0], line.split()[1:]\n",
        "    captions.append(' '.join(word.lower() for word in caption if word.isalpha() and len(word) > 1))\n",
        "    if len(captions) == 5:\n",
        "      all_captions.append(captions)\n",
        "      captions = []\n",
        "  return all_captions\n",
        "\n",
        "MAPPING = dict(zip(getIMageID(TOKEN_FILE), getCaptions(TOKEN_FILE)))\n",
        "\n",
        "def test_captions(train):\n",
        "  return [im for im in getIMageID(TOKEN_FILE) if im not in train] \n",
        "\n",
        "TEST_SET = \"/content/drive/My Drive/Colab Notebooks/Flickr8k/Flickr8k_text/Flickr_8k.testImages.txt\"\n",
        "test_images = open(TEST_SET, 'r').readlines()\n",
        "# print(test_images[0])\n",
        "train = [line.split()[0].split(\".\")[0] for line in open(\"/content/drive/My Drive/Colab Notebooks/Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt\", 'r').readlines()]\n",
        "test_actual_captions = {img + \".jpg\": MAPPING[img] for img in test_captions(train)}\n",
        "print(type(test_actual_captions))\n",
        "\n",
        "bleu_score = []\n",
        "\n",
        "for im in test_images:\n",
        "  img = \"/content/drive/My Drive/Colab Notebooks/Flickr8k/Flickr8k_Dataset/Flicker8k_Dataset/\" + im[: -1]\n",
        "  im_array = get_encoding(resnet, img)\n",
        "  predicted = predict_captions(im_array)\n",
        "  actual = test_actual_captions[im[: -1]]\n",
        "  for caption in actual:\n",
        "    if len(caption) < len(predicted):\n",
        "      for i in range(len(predicted) - len(caption)):\n",
        "        caption += \" \"\n",
        "    elif len(caption) > len(predicted):\n",
        "      for i in range(len(caption) - len(predicted)):\n",
        "        predicted += \" \"\n",
        "    # print(len(caption), len(predicted))\n",
        "    bleu_score.append('BLEU: %f' % corpus_bleu(caption, predicted))"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "CBMBqHsuEMWi",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "z = image.load_img(img)\n",
        "display(z)\n",
        "\n",
        "print(Argmax_Search)"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "somNFNsLXWgl",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "import numpy as np\n",
        "\n",
        "from google.colab import files\n",
        "from keras.preprocessing import image\n",
        "\n",
        "uploaded=files.upload()\n",
        "\n",
        "for fn in uploaded.keys():\n",
        "  display(image.load_img(fn))\n",
        "  fn = get_encoding(resnet, fn)\n",
        "  print(predict_captions(fn))"
      ],
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {
        "id": "zbtp7g9uZ1s5",
        "colab_type": "code",
        "colab": {}
      },
      "source": [
        "import matplotlib.pyplot as plt\n",
        "\n",
        "plt.plot(test_images, bleu_score)\n",
        "plt.xlabel('Images')\n",
        "plt.ylabel('Bleu score')"
      ],
      "execution_count": null,
      "outputs": []
    }
  ]
}